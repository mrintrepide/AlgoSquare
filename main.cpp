#include <iostream>
#include <array>
using namespace std;

// g++ -Ofast -march=native -flto -fprofile-generate as.cpp -o as
// ./as
// g++ -Ofast -march=native -flto -fprofile-use as.cpp -o as
// time ./as

const int c = 12;
array<array<int, 2>, 8> mouv = { { { -3, 0 }, { 0, 3 }, { 3, 0 }, { 0, -3 }, { -2, 2 }, { 2, 2 }, { 2, -2 }, { -2, -2 } } };
array<array<int, c>, c> magic = {};

void engine(int number, int x, int y)
{
	if (number > c*c)
	{
		for (int i = 0; i < c; ++i)
			for (int j = 0; j < c; ++j)
				cout << magic[i][j] << "\t";
		exit(0);
	}
	else
	{
		for (int i = 0; i < 8; ++i)
		{
			int u = x + mouv[i][0];
			int v = y + mouv[i][1];
			if (u >= 0 && u < c && v >= 0 && v < c && magic[u][v] == 0)
			{
				magic[u][v] = number;
				engine(number + 1, u, v);
				magic[u][v] = 0;
			}
		}
	}
}

int main()
{
	int number = 2;
	magic[0][0] = 1;
	engine(number, 0, 0);
	return 0;
}
